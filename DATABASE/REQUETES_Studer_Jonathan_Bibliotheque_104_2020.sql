/*
	Toutes les colonnes des trois tables (t_livres_auteurs, t_livres, t_auteurs)
*/
SELECT * FROM t_livres_auteurs AS T1
INNER JOIN t_livres AS T2 ON T2.id_livre = T1.fk_livre
INNER JOIN t_auteurs AS T3 ON T3.id_auteur = T1.fk_auteur


/*
	Seulement certaines colonnes des trois tables (t_livres_auteurs, t_livres, t_auteurs)
*/
SELECT id_livre, titre , id_auteur, nom FROM t_livres_auteurs AS T1
INNER JOIN t_auteurs AS T2 ON T2.id_auteur = T1.fk_auteur
INNER JOIN t_livres AS T3 ON T3.id_livre = T1.fk_livre


/* 	
	Permet d'afficher les trois premières colonnes de la table livre et l'id et le titre du 	livre qui leur est associé
*/
SELECT id_livre, titre , id_auteur, nom, prenom  FROM t_livres_auteurs AS T1
RIGHT JOIN t_auteurs AS T2 ON T2.id_auteur = T1.fk_auteur
LEFT JOIN t_livres AS T3 ON T3.id_livre = T1.fk_livre


/*
	Affiche tous les livres qui ont le genre littéraire 3 c'est-à-dire un genre pas encore 	défini
*/
SELECT * 
FROM t_livres 
WHERE fk_genre_litteraire = 3; 


/*
	Affiche tous les livres qui ont déjà un genre littéraire défini
*/
SELECT * 
FROM t_livres 
WHERE fk_genre_litteraire <> 3; 